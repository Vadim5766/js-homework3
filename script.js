let number;
while (number % 1 !== 0) {
  number = prompt("Введіть число!");
}
if (number < 5) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 5; i <= number; i += 5) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
}
